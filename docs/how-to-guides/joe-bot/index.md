---
title: How to work with Joe chatbot
sidebar_label: Overview
slug: /how-to-guides/joe-bot
---

## Guides
- [How to get a query execution plan (EXPLAIN)](/docs/how-to-guides/joe-bot/get-query-plan)
- [How to create an index using Joe bot](/docs/how-to-guides/joe-bot/create-index)
- [How to reset the state of a Joe session / clone](/docs/how-to-guides/joe-bot/reset-session)
- [How to get a list of active queries in a Joe session and stop long-running queries](/docs/how-to-guides/joe-bot/query-activity-and-termination)
- [How to visualize a query plan](/docs/how-to-guides/joe-bot/visualize-query-plan)
- [How to work with SQL optimization history](/docs/how-to-guides/joe-bot/sql-optimization-history)
- [How to get row counts for arbitrary SELECTs](/docs/how-to-guides/joe-bot/count-rows)
- [How to get sizes of PostgreSQL databases, tables, and indexes with psql commands](/docs/how-to-guides/joe-bot/get-database-table-index-size)

## Related
- [Joe chat commands reference](/docs/reference-guides/joe-bot-commands-reference)

[↵ Back to Guides](/docs/how-to-guides/)
