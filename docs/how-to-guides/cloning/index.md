---
title: How to work with Database Lab clones
sidebar_label: Overview
slug: /how-to-guides/cloning
---

## Guides
- [How to create Database Lab clones](/docs/how-to-guides/cloning/create-clone)
- [How to connect to Database Lab clones](/docs/how-to-guides/cloning/connect-clone)
- [How to reset Database Lab clone](/docs/how-to-guides/cloning/reset-clone)
- [How to destroy Database Lab clone](/docs/how-to-guides/cloning/destroy-clone)
- [Protect clones from manual and automatic deletion](/docs/how-to-guides/cloning/clone-protection)
